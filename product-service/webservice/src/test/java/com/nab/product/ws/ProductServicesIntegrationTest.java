/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.product.ws;

import com.nab.common.exception.NabException;
import com.nab.product.dto.CatalogBrandDto;
import com.nab.product.dto.CatalogTypeDto;
import com.nab.product.dto.ProductDto;
import com.nab.product.dto.SellerDto;
import com.nab.product.service.ProductService;
import com.nab.product.service.UserManagementService;
import com.nab.product.service.common.Constants;
import com.nab.product.ws.controller.ProductController;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

import javax.ws.rs.core.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProductServicesIntegrationTest {
    private static final String PRODUCT_UID = UUID.randomUUID().toString();
    private static final String SELLER_UID = "sellerId";

    @Mock
    private ProductService productService;

    @Mock
    private UserManagementService userManagementService;

    @InjectMocks
    private ProductController productController = new ProductController();

    @Test
    public void createProduct_should_success() throws NabException {
        ProductDto productDto = initProductDto();
        when(productService.createProduct(any(ProductDto.class), eq(SELLER_UID))).thenReturn(productDto.getId());
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(productDto.getSeller());
        Response response = productController.createProduct("sellerId", productDto);
        Assert.assertEquals(200, response.getStatus());
    }

    @Test
    public void createProduct_should_return_bad_request_invalid_seller() throws NabException {
        ProductDto productDto = initProductDto();
        when(productService.createProduct(any(ProductDto.class), eq(SELLER_UID))).thenReturn(productDto.getId());
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(null);
        try {
            productController.createProduct("sellerId", productDto);
        } catch (NabException e) {
            Assert.assertEquals("400", e.getExceptionCode().getErrorCode());
            Assert.assertEquals(Constants.INVALID_USER_ID, e.getMessageKey());
        }
    }

    private SellerDto initSeller() {
        return new SellerDto(SELLER_UID, "kim hai quach", "0974677771","kimhaiquach@gmail.com");
    }

    private ProductDto initProductDto() {
        SellerDto sellerDto = initSeller();
        ProductDto productDto = new ProductDto();
        productDto.setId(PRODUCT_UID);
        productDto.setAvailableStock(2);
        productDto.setCatalogBrand(new CatalogBrandDto(PRODUCT_UID, "LG", ""));
        productDto.setCatalogType(new CatalogTypeDto(PRODUCT_UID, "Dien tu", ""));
        productDto.setSeller(sellerDto);
        return productDto;
    }
}