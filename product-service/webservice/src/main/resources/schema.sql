create table if not exists catalog_brand
(
	Id varchar(36) not null
		primary key,
	Name varchar(128) not null,
	Description varchar(255) null
);

create table if not exists catalog_type
(
	Id varchar(36) not null
		primary key,
	Type varchar(128) not null,
	Description varchar(255) null
);

create table if not exists seller
(
	Id varchar(36) not null
		primary key,
	Name varchar(128) not null,
	Phone varchar(45) not null,
	Email varchar(255) not null
);

create table if not exists product
(
	Id varchar(36) not null
		primary key,
	Name varchar(128) not null,
	Description varchar(255) null,
	Price decimal(13,4) not null,
	Picture_Url varchar(255) null,
	Seller_Id varchar(36) not null,
	Catalog_Type_Id varchar(36) not null,
	Catalog_Brand_Id varchar(36) not null,
	Available_Stock int not null,
	Create_At bigint null,
	Last_Modified_At bigint null,
	constraint fk_catalog_brand
		foreign key (Catalog_Brand_Id) references catalog_brand (Id),
	constraint fk_catalog_type
		foreign key (Catalog_Type_Id) references catalog_type (Id),
	constraint fk_seller_id
		foreign key (Seller_Id) references seller (Id)
);

create index fk_catalog_type_idx
	on product (Catalog_Type_Id);

create index fk_seller_id_idx
	on product (Seller_Id);

create index gk_catalog_brand_idx
	on product (Catalog_Brand_Id);

