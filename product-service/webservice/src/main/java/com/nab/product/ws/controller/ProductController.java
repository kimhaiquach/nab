/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.product.ws.controller;

import com.nab.common.exception.NabException;
import com.nab.common.exception.NabExceptionCode;
import com.nab.product.dto.ProductDemandDto;
import com.nab.product.dto.ProductDto;
import com.nab.product.service.ProductService;
import com.nab.product.service.UserManagementService;
import com.nab.product.service.common.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.nab.product.service.common.Constants.INVALID_USER_ID;

@Path("/products")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserManagementService userManagementService;

    /**
     * * Returns specific product
     *
     * @param id the id of product
     * @return productDto
     */
    @GET
    @Path("/{id}")
    public Response getProductById(@PathParam("id") String id) throws NabException {
        ProductDto product = productService.getProductById(id);
        if (product == null) {
            throw new NabException(NabExceptionCode.NOT_FOUND_EXCEPTION).withMessageKey(Constants.COULD_NOT_FOUND_PRODUCT);
        }
        return Response.ok().entity(productService.getProductById(id)).build();
    }


    /**
     * * Returns a list of products
     *
     * * @return List of productDto
     */
    @GET
    public Response getAllProducts() {
        return Response.ok(productService.getAllProducts()).build();
    }

    /**
     * * Create an product
     *
     * @param userId     the id of seller
     * @param ProductDto ProductDto information.
     * @return id of product
     */
    @POST
    public Response createProduct(@NotBlank @HeaderParam("userId") String sellerId, @Valid @RequestBody ProductDto product) throws NabException {
        validateSeller(sellerId, product);
        return Response.ok(productService.createProduct(product, sellerId)).build();
    }

    /**
     * * Create an product
     *
     * @param id         the id of product
     * @param userId     the id of seller
     * @param ProductDto ProductDto information.
     * @return id of product
     */
    @PUT
    @Path("/{id}")
    public Response updateProduct(
            @PathParam("id") String id,
            @NotBlank @HeaderParam("userId") String sellerId,
            @RequestBody @Valid ProductDto product) throws NabException {
        validateSeller(sellerId, product);
        if (!id.equals(product.getId())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        productService.updateProduct(product, sellerId);
        return Response.ok().build();
    }

    /**
     * * Create an product
     *
     * @param id         the id of product
     * @param userId     the id of seller
     * @param ProductDto ProductDto information.
     * @return id of product
     */
    @PUT
    public Response updateProductAvailableStock(
            @RequestBody @Valid List<ProductDemandDto> productDemands) throws NabException {
        productService.updateProductAvalableStock(productDemands);
        return Response.ok().build();
    }


    /**
     * * Create an product
     *
     * @param id     the id of product
     * @param userId the id of seller
     * @return id of product
     */
    @DELETE
    @Path("{id}")
    public Response deleteProduct(
            @PathParam("id") String id,
            @NotBlank @HeaderParam("userId") String sellerId) throws NabException {
        productService.deleteProduct(id, sellerId);
        return Response.ok().build();
    }

    private void validateSeller(String sellerId, ProductDto product) throws NabException {
        if (product.getSeller() == null || !sellerId.equals(product.getSeller().getId()) || userManagementService.getSellerById(sellerId) == null) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(INVALID_USER_ID);
        }
    }
}
