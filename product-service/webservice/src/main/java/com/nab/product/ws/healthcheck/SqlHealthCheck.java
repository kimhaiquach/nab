/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.product.ws.healthcheck;

import com.nab.product.repository.ProductRepository;
import com.nab.product.repository.SellerUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * Created by haiquach on 2020-06-05.
 */

/**
 * Determines if URP Service are working correctly.
 */
@Component
public class SqlHealthCheck implements HealthIndicator {
    private static final String DETAILS = "details";

    /**
     * Seller Repository
     */
    @Autowired(required = false)
    private SellerUserRepository sellerRepository;

    /**
     * Product Repository
     */
    @Autowired(required = false)
    private ProductRepository productRepository;

    /**
     * This method is using for checking the status of repository
     *
     * @return the status of services
     */
    @Override
    public Health health() {
        if (productRepository == null) {
            return Health.down().withDetail(DETAILS, "missing @Autowired: productSqlRepository").build();
        }
        if (sellerRepository == null) {
            return Health.down().withDetail(DETAILS, "missing @Autowired: sellerSqlRepository").build();
        }
        return Health.up().build();
    }
}
