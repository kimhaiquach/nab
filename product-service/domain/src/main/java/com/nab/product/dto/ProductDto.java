/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class ProductDto {
    private String id;

    @NotEmpty(message = "Please provide an Product Name")
    private String name;

    private String description;

    @Min(value = 0, message = "Please select positive numbers Only")
    private BigDecimal price;
    private String pictureUrl;

    @Min(value = 0, message = "Please select positive numbers Only")
    private int availableStock;
    private SellerDto seller;
    private CatalogTypeDto catalogType;
    private CatalogBrandDto catalogBrand;
}
