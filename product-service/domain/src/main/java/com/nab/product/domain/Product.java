/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.domain;

import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
    private String pictureUrl;
    private int availableStock;
    private Long createAt;
    private Long lastModifiedAt;
    private CatalogType catalogType;
    private CatalogBrand catalogBrand;
    private Seller seller;

    @Id
    @Column(name = "Id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String getId() {
        return id;
    }

    public Product setId(String id) {
        this.id = id;
        return this;
    }


    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }


    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }


    @Column(name = "Price")
    public BigDecimal getPrice() {
        return price;
    }

    public Product setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }


    @Column(name = "Picture_Url")
    public String getPictureUrl() {
        return pictureUrl;
    }

    public Product setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
        return this;
    }


    @Column(name = "Available_Stock")
    public int getAvailableStock() {
        return availableStock;
    }

    public Product setAvailableStock(int availableStock) {
        this.availableStock = availableStock;
        return this;
    }


    @Column(name = "Create_At")
    public Long getCreateAt() {
        return createAt;
    }

    public Product setCreateAt(Long createAt) {
        this.createAt = createAt;
        return this;
    }


    @Column(name = "Last_Modified_At")
    public Long getLastModifiedAt() {
        return lastModifiedAt;
    }

    public Product setLastModifiedAt(Long lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        return availableStock == product.availableStock &&
                Objects.equals(id, product.id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(description, product.description) &&
                Objects.equals(price, product.price) &&
                Objects.equals(pictureUrl, product.pictureUrl) &&
                Objects.equals(createAt, product.createAt) &&
                Objects.equals(lastModifiedAt, product.lastModifiedAt) &&
                Objects.equals(seller, product.seller);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, pictureUrl, availableStock, createAt, lastModifiedAt, seller);
    }

    @ManyToOne
    @JoinColumn(name = "Catalog_Type_Id", referencedColumnName = "Id")
    public CatalogType getCatalogType() {
        return catalogType;
    }

    public Product setCatalogType(CatalogType catalogType) {
        this.catalogType = catalogType;
        return this;
    }

    @ManyToOne
    @JoinColumn(name = "Catalog_Brand_Id", referencedColumnName = "Id")
    public CatalogBrand getCatalogBrand() {
        return catalogBrand;
    }

    public Product setCatalogBrand(CatalogBrand catalogBrand) {
        this.catalogBrand = catalogBrand;
        return this;
    }


    @ManyToOne
    @JoinColumn(name = "Seller_Id", referencedColumnName = "Id")
    public Seller getSeller() {
        return seller;
    }

    public Product setSeller(Seller seller) {
        this.seller = seller;
        return this;
    }
}
