/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.event;

import java.io.Serializable;

public class ProductMessageChange implements Serializable {
    private static final long serialVersionUID = 1L;

    private String userId;
    private String userName;
    private String valueType;
    private String from;
    private String to;

    public ProductMessageChange() {
    }

    public ProductMessageChange(String userId, String userName, String valueType, String from, String to) {
        super();
        this.userId = userId;
        this.userName = userName;
        this.valueType = valueType;
        this.from = from;
        this.to = to;
    }

    @Override
    public String toString() {
        return "ProductMessageChange{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", valueType='" + valueType + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}
