/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.domain;

import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "seller")
@NoArgsConstructor
@AllArgsConstructor
public class Seller {
    private String id;
    private String name;
    private String phone;
    private String email;

    @Id
    @Column(name = "Id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String getId() {
        return id;
    }

    public Seller setId(String id) {
        this.id = id;
        return this;
    }


    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public Seller setName(String name) {
        this.name = name;
        return this;
    }


    @Column(name = "Phone")
    public String getPhone() {
        return phone;
    }

    public Seller setPhone(String phone) {
        this.phone = phone;
        return this;
    }


    @Column(name = "Email")
    public String getEmail() {
        return email;
    }

    public Seller setEmail(String email) {
        this.email = email;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Seller seller = (Seller) o;
        return Objects.equals(id, seller.id) &&
                Objects.equals(name, seller.name) &&
                Objects.equals(phone, seller.phone) &&
                Objects.equals(email, seller.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, phone, email);
    }
}
