/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.event;

import java.io.Serializable;


public class MessageEvent implements Serializable {
    private String userId;
    private String actorName;
    private String eventType;
    private String propertyChange;
    private Object oldValue;
    private Object newValue;

    public MessageEvent() {
    }

    public MessageEvent(String userId, String actorName, String eventType, ObjectPropertyChange propertyChange) {
        this.userId = userId;
        this.actorName = actorName;
        this.eventType = eventType;
        this.propertyChange = propertyChange.getPropertyName();
        this.oldValue = propertyChange.getOldValue();
        this.newValue = propertyChange.getNewValue();
    }

    public String getPropertyChange() {
        return propertyChange;
    }

    public MessageEvent setPropertyChange(String propertyChange) {
        this.propertyChange = propertyChange;
        return this;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public MessageEvent setOldValue(Object oldValue) {
        this.oldValue = oldValue;
        return this;
    }

    public Object getNewValue() {
        return newValue;
    }

    public MessageEvent setNewValue(Object newValue) {
        this.newValue = newValue;
        return this;
    }

    public String getActorName() {
        return actorName;
    }

    public MessageEvent setActorName(String actorName) {
        this.actorName = actorName;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public MessageEvent setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getEventType() {
        return eventType;
    }

    public MessageEvent setEventType(String eventType) {
        this.eventType = eventType;
        return this;
    }
}
