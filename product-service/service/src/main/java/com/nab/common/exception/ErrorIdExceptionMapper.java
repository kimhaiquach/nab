/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.common.exception;


import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public abstract class ErrorIdExceptionMapper<T extends Throwable> implements ExceptionMapper<T> {
    private String errorId;

    public ErrorIdExceptionMapper() {
    }

    public void withErrorId(String errorId) {
        this.errorId = errorId;
    }

    public abstract Response toResponse(T exception, String errorId);

    public Response toResponse(T exception) {
        if (this.errorId == null) {
            this.errorId = this.generateErrorId();
        }

        return this.toResponse(exception, this.errorId);
    }

    protected String generateErrorId() {
        long millis = System.currentTimeMillis();
        return Long.toString(millis, 36).toUpperCase();
    }
}
