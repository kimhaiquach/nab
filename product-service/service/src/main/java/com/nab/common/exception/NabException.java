/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.common.exception;


import java.text.MessageFormat;

public class NabException extends Exception {
    private static final long serialVersionUID = 1L;
    private static final String MESSAGE_PATTERN = "{0} : {1}.";
    private final NabExceptionCode nabExceptionCode;
    private String messageKey;
    private int status;
    private transient Object detail;
    private String errorId;

    public NabException(NabExceptionCode nabExceptionCode) {
        super(MessageFormat.format(MESSAGE_PATTERN, nabExceptionCode.getErrorCode(), "Generic exception"));
        this.nabExceptionCode = nabExceptionCode;
    }

    public NabExceptionCode getExceptionCode() {
        return this.nabExceptionCode;
    }

    public NabException withMessageKey(String messageKey) {
        this.messageKey = messageKey;
        return this;
    }

    public NabException withDetail(Object detail) {
        this.detail = detail;
        return this;
    }

    public NabException withErrorId(String errorId) {
        this.errorId = errorId;
        return this;
    }

    public String getErrorId() {
        return this.errorId;
    }

    public String getMessageKey() {
        return this.messageKey;
    }

    public Object getDetail() {
        return this.detail;
    }

    public int getStatus() {
        return status;
    }

    public NabException setStatus(int status) {
        this.status = status;
        return this;
    }
}
