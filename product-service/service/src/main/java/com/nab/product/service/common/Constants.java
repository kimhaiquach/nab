/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service.common;

public class Constants {
    public static final String COULD_NOT_FOUND_PRODUCT = "com.nab.product.exception.could_not_found_product";
    public static final String INVALID_USER_ID = "com.nab.product.exception.invalid_user_id";
    public static final String COULD_NOT_DELETE_PRODUCT_OF_OTHER = "com.nab.product.exception.could_not_delete_product_of_other";

    public static final String PRODUCT_UNAVAILABLE = "com.nab.product.exception.unavailable_product";

    public static final String PRODUCT_PRICE= "Price";
    public static final String UPDATE_PRODUCT= "Update Product";

    private Constants() {
    }
}
