/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service.mapper;

import com.nab.product.domain.Seller;
import com.nab.product.dto.SellerDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class SellerMapper {
    public static final SellerMapper MAPPER = Mappers.getMapper(SellerMapper.class);

    public abstract SellerDto toDto(Seller model);

    public abstract Seller toDto(SellerDto dto);
}
