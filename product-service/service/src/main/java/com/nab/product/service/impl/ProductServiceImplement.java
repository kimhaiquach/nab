/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.common.exception.NabException;
import com.nab.common.exception.NabExceptionCode;
import com.nab.product.domain.Product;
import com.nab.product.dto.ProductDemandDto;
import com.nab.product.dto.ProductDto;
import com.nab.product.dto.SellerDto;
import com.nab.product.event.MessageEvent;
import com.nab.product.event.ObjectPropertyChange;
import com.nab.product.repository.ProductRepository;
import com.nab.product.service.ProductService;
import com.nab.product.service.UserManagementService;
import com.nab.product.service.common.Constants;
import com.nab.product.service.mapper.ProductMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.jms.Queue;

import static com.nab.product.service.common.Constants.PRODUCT_PRICE;
import static com.nab.product.service.common.Constants.UPDATE_PRODUCT;

@Service
public class ProductServiceImplement implements ProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImplement.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private Queue queue;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<ProductDto> getAllProducts() {
        return productRepository.findAll().stream().map(ProductMapper.MAPPER::toEntity).collect(Collectors.toList());
    }

    @Override
    public ProductDto getProductById(String id) {
        return productRepository.findById(id).map(ProductMapper.MAPPER::toEntity).orElse(null);
    }

    @Override
    public String createProduct(ProductDto productDto, String sellerId) {
        Product product = productRepository.save(ProductMapper.MAPPER.toDto(productDto));
        return product.getId();
    }

    public void updateProduct(ProductDto productDto, String sellerId) throws NabException {
        List<ObjectPropertyChange> objectPropertyChanges = new ArrayList<>();
        SellerDto seller = userManagementService.getSellerById(sellerId);
        if (seller == null) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.INVALID_USER_ID);
        }
        Optional<Product> optionalProduct = productRepository.findById(productDto.getId());
        if (!optionalProduct.isPresent()) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_FOUND_PRODUCT);
        }

        Product originProduct = optionalProduct.get();
        productRepository.save(ProductMapper.MAPPER.toDto(productDto));

        if (optionalProduct.get().getPrice().compareTo(productDto.getPrice()) != 0) {
            objectPropertyChanges.add(new ObjectPropertyChange(PRODUCT_PRICE, originProduct.getPrice(), productDto.getPrice()));
            sendProductUpdateEvent(seller, UPDATE_PRODUCT, objectPropertyChanges);
        }
    }

    @Override
    public void updateProductAvalableStock(List<ProductDemandDto> productDemands) throws NabException {
        List<Product> products = new ArrayList<>();
        int availableStock = 0;
        for (ProductDemandDto productDemandDTO : productDemands){
            Optional<Product> optionalProduct = productRepository.findById(productDemandDTO.getProductId());
            if (!optionalProduct.isPresent()) {
                throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_FOUND_PRODUCT);
            }
            Product product = optionalProduct.get();
            availableStock = product.getAvailableStock();
            if (availableStock>= productDemandDTO.getRequiredAmount()){
                product.setAvailableStock(availableStock-productDemandDTO.getRequiredAmount());
            } else {
                throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.PRODUCT_UNAVAILABLE);
            }
            products.add(product);
        }

        productRepository.saveAll(products);
    }

    @Override
    public void deleteProduct(String id, String sellerId) throws NabException {
        //verify user
        getSellerById(sellerId);
        Optional<Product> product = productRepository.findById(id);
        if (!product.isPresent()) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_FOUND_PRODUCT);
        }
        if (!product.get().getSeller().getId().equals(sellerId)) {
            throw new NabException(NabExceptionCode.FORBIDDEN_EXCEPTION).withMessageKey(Constants.COULD_NOT_DELETE_PRODUCT_OF_OTHER);
        }
        productRepository.delete(product.get());
    }

    private void sendProductUpdateEvent(SellerDto seller, String eventType, List<ObjectPropertyChange> propertyChanges) {
        MessageEvent messageEvent = new MessageEvent(seller.getId(), seller.getName(), eventType, propertyChanges.get(0));
        String dtoAsString = "";
        try {
            dtoAsString = objectMapper.writeValueAsString(messageEvent);
        } catch (JsonProcessingException e) {
            LOGGER.error("could not convert message, userId:{}, eventType:{}", seller.getId(), eventType, e);
        }
        jmsTemplate.convertAndSend(queue, dtoAsString);
    }

    private SellerDto getSellerById(String sellerId) throws NabException {
        SellerDto seller = userManagementService.getSellerById(sellerId);
        if (seller == null) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.INVALID_USER_ID);
        }
        return seller;
    }
}