/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service.impl;

import com.nab.product.dto.SellerDto;
import com.nab.product.repository.SellerUserRepository;
import com.nab.product.service.UserManagementService;
import com.nab.product.service.mapper.SellerMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManagementServiceImplement implements UserManagementService {

    @Autowired
    private SellerUserRepository sellerRepository;

    @Override
    public SellerDto getSellerById(String id) {
        return sellerRepository.findById(id).map(SellerMapper.MAPPER::toDto).orElse(null);
    }
}
