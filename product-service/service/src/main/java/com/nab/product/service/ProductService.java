/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service;

import com.nab.product.dto.ProductDemandDto;
import com.nab.product.dto.ProductDto;
import com.nab.common.exception.NabException;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAllProducts();

    ProductDto getProductById(String id) throws NabException;

    String createProduct(ProductDto product, String sellerId);

    void deleteProduct(String id, String sellerId) throws NabException;

    void updateProduct(ProductDto product, String sellerId) throws NabException;

    void updateProductAvalableStock(List<ProductDemandDto> productDemands) throws NabException;
}
