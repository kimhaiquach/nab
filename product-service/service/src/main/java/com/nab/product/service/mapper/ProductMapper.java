/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service.mapper;

import com.nab.product.domain.Product;
import com.nab.product.dto.ProductDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class ProductMapper {
    public static final ProductMapper MAPPER = Mappers.getMapper(ProductMapper.class);

    public abstract ProductDto toEntity(Product model);

    public abstract Product toDto(ProductDto dto);
}
