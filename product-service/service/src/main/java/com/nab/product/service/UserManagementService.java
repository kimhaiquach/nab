/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service;

import com.nab.product.dto.SellerDto;

/**
 * Seller Service: this service should be manage by User-Management microservice instead of product service
 * I put here because save time for preparation
 */
public interface UserManagementService {
    SellerDto getSellerById(String id);
}
