/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.common.exception.NabException;
import com.nab.product.domain.CatalogBrand;
import com.nab.product.domain.CatalogType;
import com.nab.product.domain.Product;
import com.nab.product.domain.Seller;
import com.nab.product.dto.ProductDto;
import com.nab.product.dto.SellerDto;
import com.nab.product.event.MessageEvent;
import com.nab.product.repository.ProductRepository;
import com.nab.product.service.common.Constants;
import com.nab.product.service.impl.ProductServiceImplement;
import com.nab.product.service.impl.UserManagementServiceImplement;
import com.nab.product.service.mapper.ProductMapper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.jms.Queue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProductServiceTest {
    private static final String PRODUCT_UID = UUID.randomUUID().toString();

    private static final String SELLER_UID = UUID.randomUUID().toString();

    @Mock
    private UserManagementServiceImplement userManagementService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private JmsTemplate jmsTemplate;

    @Mock
    private Queue queue;

    @InjectMocks
    private ProductService productService = new ProductServiceImplement();

    @Test
    public void testGetAllProducts() {
        when(productRepository.findAll()).thenReturn(initProducts());
        List<ProductDto> productDtoList = productService.getAllProducts();
        Assert.assertEquals(1, productDtoList.size());
    }

    @Test
    public void testUpdateProduct_should_throw_bad_request_invalid_seller() {
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(null);
        when(productRepository.findAll()).thenReturn(initProducts());
        ProductDto productDto = ProductMapper.MAPPER.toEntity(initProducts().get(0));
        try {
            productService.updateProduct(productDto, SELLER_UID);
        } catch (NabException e) {
            Assert.assertEquals("400", e.getExceptionCode().getErrorCode());
            Assert.assertEquals(Constants.INVALID_USER_ID, e.getMessageKey());
        }
    }

    @Test
    public void testUpdateProduct_should_throw_bad_request_could_not_found_product() {
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(new SellerDto());
        Optional<Product> optionalProduct = Optional.empty();
        when(productRepository.findById(PRODUCT_UID)).thenReturn(optionalProduct);
        ProductDto productDto = ProductMapper.MAPPER.toEntity(initProducts().get(0));
        try {
            productService.updateProduct(productDto, SELLER_UID);
        } catch (NabException e) {
            Assert.assertEquals("400", e.getExceptionCode().getErrorCode());
            Assert.assertEquals(Constants.COULD_NOT_FOUND_PRODUCT, e.getMessageKey());
        }
    }

    @Test
    public void updateProduct_price_should_success() throws NabException, JsonProcessingException {
        Product product = initProducts().get(0);
        Optional<Product> optionalProduct = Optional.of(product);
        String messageProductPriceChanged = "message product price changed";

        when(objectMapper.writeValueAsString(any(MessageEvent.class))).thenReturn(messageProductPriceChanged);
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(new SellerDto());
        when(productRepository.findById(PRODUCT_UID)).thenReturn(optionalProduct);
        ProductDto productDto = ProductMapper.MAPPER.toEntity(initProducts().get(0));

        //update new price
        productDto.setPrice(BigDecimal.valueOf(99));
        //when
        productService.updateProduct(productDto, SELLER_UID);

        //then
        Mockito.verify(productRepository, times(1)).save(any(Product.class));
        Mockito.verify(jmsTemplate, times(1)).convertAndSend(eq(queue), eq(messageProductPriceChanged));
    }

    @Test
    public void delete_product_throw_bad_request_could_not_found_product() {
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(new SellerDto());
        Optional<Product> optionalProduct = Optional.ofNullable(null);
        when(productRepository.findById(PRODUCT_UID)).thenReturn(optionalProduct);

        try {
            productService.deleteProduct(PRODUCT_UID, SELLER_UID);
        } catch (NabException e) {
            Assert.assertEquals("400", e.getExceptionCode().getErrorCode());
            Assert.assertEquals(Constants.COULD_NOT_FOUND_PRODUCT, e.getMessageKey());
        }
    }

    @Test
    public void delete_product_throw_forbidden_could_not_delete_product_of_other() {
        Product product = initProducts().get(0);
        product.getSeller().setId("newId");
        Optional<Product> optionalProduct = Optional.of(product);
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(new SellerDto());
        when(productRepository.findById(PRODUCT_UID)).thenReturn(optionalProduct);

        //when
        try {
            productService.deleteProduct(PRODUCT_UID, SELLER_UID);
        } catch (NabException e) {
            Assert.assertEquals("403", e.getExceptionCode().getErrorCode());
            Assert.assertEquals(Constants.COULD_NOT_DELETE_PRODUCT_OF_OTHER, e.getMessageKey());
        }
    }

    @Test
    public void delete_product_success() throws NabException {
        Product product = initProducts().get(0);
        Optional<Product> optionalProduct = Optional.of(product);
        when(userManagementService.getSellerById(SELLER_UID)).thenReturn(new SellerDto());
        when(productRepository.findById(PRODUCT_UID)).thenReturn(optionalProduct);
        productService.deleteProduct(PRODUCT_UID, SELLER_UID);
        //then
        Mockito.verify(productRepository, times(1)).delete(any(Product.class));
    }


    private List<Product> initProducts() {
        Seller seller = initSeller();
        Product product = new Product();
        product.setAvailableStock(2);
        product.setPrice(BigDecimal.valueOf(10));
        product.setId(PRODUCT_UID);
        product.setAvailableStock(2);
        product.setCatalogBrand(new CatalogBrand(PRODUCT_UID, "LG", ""));
        product.setCatalogType(new CatalogType(PRODUCT_UID, "Dien tu", ""));
        product.setSeller(seller);
        return Arrays.asList(product);
    }

    private Seller initSeller() {
        return new Seller(SELLER_UID, "hquach", "kimhaiquach@gmail.com", "0974677771");
    }
}
