/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.product.repository;

import com.nab.product.domain.Seller;

import org.springframework.data.jpa.repository.JpaRepository;


public interface SellerUserRepository extends JpaRepository<Seller, String> {
}
