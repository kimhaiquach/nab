EVENT Store ProcessorApp
----
Build Spring Boot Microservice to listen event from when product change Price and update change information into mongodb
These data will be used In the future like Report
 

### INTRODUCTION

Techonology stack:
- Java 8
- Spring Boot 
- Spring Jpa 
- h2 embedded
- Spring Data Mongo
- Lombok
- MapStruct
- ActiveMQ
 
### BUILD
To build
```bash
mvn clean install
```

### DEPLOYMENT
To start this service locally
```bash
 cd webservice
 mvn spring-boot:run
```

### USAGE
When you run this app you can access its features using several RESTful endpoints. Note - this is only a SMALL sample of the endpoints available. e.g. when running locally:

GET http://localhost:6868/product-service/products - return list all products 

GET http://localhost:6868/product-service/products/00000000-0000-0000-0001-000000000001  return single product by orderId

POST http://localhost:6868/product-service/products/orders to create new order.

Header:"userId":00000000-0000-0001-0000-000000000001 and requestBody ProductDto has data like below:
```bash
{
  "name": "Iphone",
  "description": "samsung s10 is cool smartphone",
  "price": 15000000.0000,
  "availableStock": 999,
  "seller": {
    "id": "00000000-0000-0001-0000-000000000001",
    "name": "Kevil",
    "phone": "096868686868",
    "email": "kimhaiquach@mail.com"
  },
  "catalogType": {
    "id": "00000000-0000-0000-0001-000000000001",
    "type": "Điện Tử",
    "description": "Điện Tử"
  },
  "catalogBrand": {
    "id": "00000000-0000-0000-0000-000000000001",
    "name": "Toshiba",
    "description": "Toshiba"
  }
}
```

PUT http://localhost:6868/product-service/products/{id} to update an Product with userId header and request body like above 

DELETE http://localhost:6868/product-service/products/{id} to delete an product

http://localhost:9898/health This returns the current health of the app, it is provided by Spring Boot Actuator. This and all other actuator endpoints that actuator provides are available immediately.