/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.eventstore.application;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.eventstore.domain.ObjectPropertyChange;
import com.nab.eventstore.domain.ProductEvent;
import com.nab.eventstore.repository.EventStoreMongoRepository;
import com.nab.eventstore.repository.ProductRepository;
import com.nab.product.domain.Product;
import com.nab.product.dto.DeletedOrderDto;
import com.nab.product.event.MessageEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class ProductEventService {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EventStoreMongoRepository eventStoreMongoRepository;

    @JmsListener(destination = "productChange-queue", containerFactory = "myFactory")
    public void receiveMessage(String productChangeEvent) throws IOException {
        MessageEvent dto = objectMapper.readValue(productChangeEvent, MessageEvent.class);

        ProductEvent productEvent = convertToEvent(dto);
        eventStoreMongoRepository.save(productEvent);
    }

    @JmsListener(destination = "order-cancel-queue", containerFactory = "myFactory")
    public void updateProductAvailableStock(String updateAvailableStockMessage) throws IOException {
        List<DeletedOrderDto> deletedOrderDtos = objectMapper.readValue(updateAvailableStockMessage, new TypeReference<List<DeletedOrderDto>>() {
        });
        List<Product> products = new ArrayList<>();
        deletedOrderDtos.stream().forEach(deletedOrderDto -> {
            Optional<Product> optionalProduct = productRepository.findById(deletedOrderDto.getProductId());
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                product.setAvailableStock(product.getAvailableStock() + deletedOrderDto.getAmount());
                products.add(product);
            }
        });
        productRepository.saveAll(products);
    }

    private ProductEvent convertToEvent(MessageEvent messageEvent) {
        List<ObjectPropertyChange> objectPropertyChanges = Arrays.asList(new ObjectPropertyChange(messageEvent.getPropertyChange(), messageEvent.getOldValue(), messageEvent.getNewValue()));
        return new ProductEvent(messageEvent.getUserId(), messageEvent.getEventType(), objectPropertyChanges);
    }
}
