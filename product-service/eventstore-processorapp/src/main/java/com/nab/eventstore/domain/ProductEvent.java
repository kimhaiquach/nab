/*
 * Copyright (c) 2017-2019 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.eventstore.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * This is the Event Store model and representation that is persisted into the eventstore repository.
 *
 * @author hdornellas
 */
@Document(collection = "ProductEvent")
public class ProductEvent {

    /**
     * This is the UUID generated from the source-feature that created the message.
     */
    @Id
    private String eventId;

    /**
     * The required UTC timestamp of the event
     */
    private Date ts;

    /**
     * The userUid string value that the entity belonged to.
     */
    private String userUid;


    /**
     * This is optional and is the changes from previous instance of the object this event represents.
     * example:
     * <pre>
     *     In a deviceUploadUpdate event, we can have the DeviceName being changed from saruman to ABT11234
     *     objectProperties: [ { "propertyName":"DeviceName", "oldValue":"saruman", "newValue":"ABT11234" } ]
     * </pre>
     * The propertyName can take complex forms if there are nested properties: "systemInformation.DeviceName", for example.
     */
    private List<ObjectPropertyChange> objectPropertyChanges = new ArrayList<>();

    /**
     * This is the action that governs the current activity instance.
     * Typical values could be: updateProductPrice, createdProduct...
     */
    private String eventType;


    public ProductEvent() {
    }

    /**
     * main creation construction
     *
     * @param userUid               the account uuid
     * @param eventType             the eventtype
     * @param objectPropertyChanges misc details
     */
    public ProductEvent(String userUid, String eventType, List<ObjectPropertyChange> objectPropertyChanges) {
        this.eventId = UUID.randomUUID().toString();
        this.userUid = userUid;
        this.eventType = eventType;
        this.objectPropertyChanges = objectPropertyChanges;
        this.ts = Calendar.getInstance().getTime();
    }

    /**
     * {@link ProductEvent#eventId}
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * {@link ProductEvent#eventId}
     */
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    /**
     * {@link ProductEvent##ts}
     */
    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    /**
     * {@link ProductEvent#userUid}
     */
    public String getUserUid() {
        return userUid;
    }

    /**
     * {@link ProductEvent#userUid}
     */
    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    /**
     * {@link ProductEvent#eventType}
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * {@link ProductEvent#eventType}
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * {@link ProductEvent#objectPropertyChanges}
     */
    public List<ObjectPropertyChange> getObjectPropertyChanges() {
        return objectPropertyChanges;
    }

    /**
     * {@link ProductEvent#objectPropertyChanges}
     */
    public void setObjectPropertyChanges(List<ObjectPropertyChange> objectPropertyChanges) {
        this.objectPropertyChanges = objectPropertyChanges;
    }

}
