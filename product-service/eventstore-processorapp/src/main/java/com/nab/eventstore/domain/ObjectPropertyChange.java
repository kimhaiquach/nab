package com.nab.eventstore.domain;

import org.springframework.data.mongodb.core.mapping.Field;

public class ObjectPropertyChange {
    @Field("pn")
    private String propertyName;
    @Field("oVal")
    private Object oldValue;
    @Field("nVal")
    private Object newValue;

    public ObjectPropertyChange() {
    }

    public ObjectPropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public void setOldValue(Object oldValue) {
        this.oldValue = oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }

    public void setNewValue(Object newValue) {
        this.newValue = newValue;
    }
}
