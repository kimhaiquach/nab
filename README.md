# ECommerce Microservices with Spring Boot

### Microservices in System:

- **Product Microservice**
    - Responsibilities:
        - Manage which products are available in the system
        - Get available amount for each product from **Order-Microservice via REST API**
        - UI for CRUD operations on products
    - Source: 
    - Testing: http://localhost:6868/product-service

- **Product Eventstore Processorapp Microservice**
    - Responsibilities:
        - Handle **JMS message to when product change price from Product-Microservice**
        - Handle **JMS message about deleted order from Order-Microservice** in order to increase the available amount  
    - Source: _this repository

- **Order Microservice**
    - Responsibilities:
        - Manage orders for products
        - update the available products from **Product-Microservice REST API**  for decreasing the available amount of the product
        - In case of deleting order send **JMS message to Product-Eventstore-Processorapp Microservice** for increasing the available amount of the product
        - UI for viewing available products and managing orders
    - Source: 
    - Testing: http://localhost:6868/product-service
    
### Require to install ActiveMQ on local:
   Because System is using active mq to communicate asynchronous message between multiple services so we need an activeMQ on local. 
 
   I have installed ActiveMQ by downloading here ```https://activemq.apache.org/components/classic/download/```
   here.  Once you have installed, the ActiveMQ server should be available at http://localhost:8161/admin and we will see the following welcome page.
   Note: Service are using default activeMQ credential ``admin/admin`` you can change on application.yml file


More document information to install activemq:  https://activemq.apache.org/installation