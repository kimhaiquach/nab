/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.common.exception;

import com.fasterxml.jackson.databind.JsonMappingException;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public final class NabExceptionMapper extends ErrorIdExceptionMapper<Exception> {
        private static final String UTF_8 = "UTF-8";
    public Response toResponse(Exception e, String errorId) {
        if (e instanceof NabException) {
            NabException nabException = (NabException) e;
            if (nabException.getErrorId() != null) {
                errorId = nabException.getErrorId();
            }

            return this.createResponseFromNabException(nabException, errorId);
        } else if (e instanceof JsonMappingException) {
            return this.createResponseFromJsonMappingException((JsonMappingException) e, errorId);
        } else {
            return e instanceof WebApplicationException ? ((WebApplicationException) e).getResponse() : this.createResponseFromException(e, errorId);
        }
    }

    private Response createResponseFromJsonMappingException(JsonMappingException e, String errorId) {
        Response.Status status = Response.Status.BAD_REQUEST;
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(status.getStatusCode());
        errorResponse.setErrorId(errorId);
        return Response.status(status).entity(errorResponse).type(MediaType.APPLICATION_JSON_TYPE.withCharset(UTF_8)).build();
    }

    private Response createResponseFromNabException(NabException e, String errorId) {
        NabExceptionCode exceptionCode = e.getExceptionCode();
        Response.StatusType status = Response.Status.INTERNAL_SERVER_ERROR;
        if (exceptionCode != null) {
            switch (exceptionCode) {
                case INVALID_PROPERTY_EXCEPTION:
                case REQUIRED_PROPERTY_EXCEPTION:
                case DUPLICATED_PROPERTY_NAME_EXCEPTION:
                case USER_UID_MATCH_VALIDATION_EXCEPTION:
                case INVALID_STATE_EXCEPTION:
                case BAD_REQUEST_EXCEPTION:
                    status = Response.Status.BAD_REQUEST;
                    break;
                case CONFLICT_EXCEPTION:
                    status = Response.Status.CONFLICT;
                    break;
                case NOT_FOUND_EXCEPTION:
                    status = Response.Status.NOT_FOUND;
                    break;
                case UNAUTHORIZED_EXCEPTION:
                    status = Response.Status.UNAUTHORIZED;
                    break;
                case FORBIDDEN_EXCEPTION:
                    status = Response.Status.FORBIDDEN;
                    break;
                default:
                    status = Response.Status.INTERNAL_SERVER_ERROR;
            }
        }

        ErrorResponse<Object> errorResponse = new ErrorResponse();
        errorResponse.setStatus((status).getStatusCode());
        errorResponse.setErrorId(errorId);
        errorResponse.setMessageKey(e.getMessageKey());
        errorResponse.setDetail(e.getDetail());
        return Response.status(status).entity(errorResponse).type(MediaType.APPLICATION_JSON_TYPE.withCharset(UTF_8)).build();
    }

    private Response createResponseFromException(Exception e, String errorId) {
        Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
        if (NotFoundException.class.isAssignableFrom(e.getClass())) {
            status = Response.Status.NOT_FOUND;
        } else if (IllegalArgumentException.class.isAssignableFrom(e.getClass())) {
            status = Response.Status.BAD_REQUEST;
        }

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(status.getStatusCode());
        errorResponse.setErrorId(errorId);
        return Response.status(status).entity(errorResponse).type(MediaType.APPLICATION_JSON_TYPE.withCharset(UTF_8)).build();
    }
}
