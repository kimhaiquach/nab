/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.common;

public class Constants {
    public static final String COULD_NOT_FOUND_ORDER = "com.nab.product.exception.could_not_found_order";

    public static final String COULD_NOT_CANCEL_OTHER_OF_OTHER = "com.nab.product.exception.could_not_cancel_other_of_other";
    public static final String INVALID_ORDER_STATUS = "com.nab.product.exception.invalid_order_status";

    public static final String DELETE_NON_EXIST_ORDER = "com.nab.product.exception.delete_not_exit_order";
    public static final String COULD_NOT_CANCEL_ORDER_AT_THIS_MOMENT = "com.nab.product.exception.could_not_cancel_order_at_this_moment";

    public static final String COULD_NOT_CHANGE_SHIPPING_ORDER = "com.nab.product.exception.could_not_change_shipping_order_to_pending";

    public static final String COULD_NOT_CHANGE_COMPLETE_ORDER = "com.nab.product.exception.could_not_change_complete_order";

    public static final String COULD_NOT_CREATE_ORDER = "com.nab.product.exception.could_not_create_order";

    public static final String PRODUCT_HOST_ADDRESS = "http://localhost:6868";
    public static final String PRODUCTS_URI = "/product-service/products";
    public static final String ORDER_CANCEL_QUEUE_NAME = "order-cancel-queue";

    private Constants() {
    }
}
