/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.common.exception;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse<T> {
    private int status;
    private String messageKey;
    private String errorId;
    private T detail;

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessageKey() {
        return this.messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public T getDetail() {
        return this.detail;
    }

    public void setDetail(T detail) {
        this.detail = detail;
    }

    public String getErrorId() {
        return this.errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public String toString() {
        return "ErrorResponse{status=" + this.status + ", messageKey='" + this.messageKey + '\'' + ", errorId=" + this.errorId + ", detail=" + this.detail + '}';
    }
}