/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.common.exception;

public enum NabExceptionCode {
    INVALID_PROPERTY_EXCEPTION("991"),
    REQUIRED_PROPERTY_EXCEPTION("992"),
    DUPLICATED_PROPERTY_NAME_EXCEPTION("993"),
    USER_UID_MATCH_VALIDATION_EXCEPTION("994"),
    INVALID_STATE_EXCEPTION("995"),
    NOT_FOUND_EXCEPTION("996"),
    BAD_REQUEST_EXCEPTION("400"),
    UNAUTHORIZED_EXCEPTION("401"),
    FORBIDDEN_EXCEPTION("403"),
    CONFLICT_EXCEPTION("409");


    private String errorCode;

    private NabExceptionCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String toString() {
        return "ApplicationExceptionCode{errorCode=" + this.errorCode + '}';
    }
}
