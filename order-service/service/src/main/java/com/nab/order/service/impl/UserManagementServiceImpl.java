/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.service.impl;

import com.nab.order.dto.BuyerDto;
import com.nab.order.repository.BuyerUserRepository;
import com.nab.order.service.UserManagementService;
import com.nab.order.service.mapper.BuyerMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManagementServiceImpl implements UserManagementService {

    @Autowired
    private BuyerUserRepository buyerRepository;

    @Override
    public BuyerDto getUserById(String id) {
        return buyerRepository.findById(id).map(BuyerMapper.MAPPER::entityToDto).orElse(null);
    }
}

