/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.service;

import com.nab.order.dto.BuyerDto;

public interface UserManagementService {
    BuyerDto getUserById(String buyerId);
}
