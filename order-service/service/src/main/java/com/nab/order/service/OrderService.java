/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.service;

import com.nab.common.exception.NabException;
import com.nab.order.dto.OrderDto;

import java.util.List;

public interface OrderService {
    List<OrderDto> getOrderByBuyerId(String buyerId);

    OrderDto getOrderById(String id) throws NabException;

    String createOrder(OrderDto order, String buyerId) throws NabException;

    void updateStatus(String orderStatus, String orderId) throws NabException;

    void cancelOrder(String id, String buyerId) throws NabException;
}
