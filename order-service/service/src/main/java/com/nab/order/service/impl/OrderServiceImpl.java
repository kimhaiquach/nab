/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nab.common.Constants;
import com.nab.common.exception.NabException;
import com.nab.common.exception.NabExceptionCode;
import com.nab.order.domain.Order;
import com.nab.order.domain.OrderItem;
import com.nab.order.dto.DeletedOrderDto;
import com.nab.order.dto.OrderDto;
import com.nab.order.dto.OrderStatus;
import com.nab.order.dto.ProductDemandDTO;
import com.nab.order.repository.OrderRepository;
import com.nab.order.service.OrderService;
import com.nab.order.service.mapper.OrderMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.jms.Queue;

@Service
public class OrderServiceImpl implements OrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Queue queue;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<OrderDto> getOrderByBuyerId(String buyerId) {
        return orderRepository.findOrderByBuyerId(buyerId).stream().map(OrderMapper.MAPPER::entityToDto).collect(Collectors.toList());
    }

    @Override
    public OrderDto getOrderById(String id) {
        return orderRepository.findById(id).map(OrderMapper.MAPPER::entityToDto).orElse(null);
    }

    @Override
    public String createOrder(OrderDto orderDto, String buyerId) throws NabException {
        BigDecimal totalPrice = BigDecimal.valueOf(0);
        Order order = OrderMapper.MAPPER.dtoToEntity(orderDto);
        List<ProductDemandDTO> productDemands = new ArrayList<>();
        fillOrderInfor(order);
        for (OrderItem orderItem : order.getOrderItems()) {
            productDemands.add(new ProductDemandDTO(orderItem.getProductId(), orderItem.getUnits()));
            orderItem.setOrder(order);
            totalPrice = totalPrice.add(orderItem.getUnitPrice().multiply(BigDecimal.valueOf(orderItem.getUnits())));
        }
        updateProductDemand(productDemands);
        order.setTotalPrice(totalPrice);
        Order product = orderRepository.save(order);
        return product.getId();
    }

    @Override
    public void updateStatus(String orderStatus, String orderId) throws NabException {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (!optionalOrder.isPresent()) {
            throw new NabException(NabExceptionCode.NOT_FOUND_EXCEPTION).withMessageKey(Constants.COULD_NOT_FOUND_ORDER);
        }
        if (OrderStatus.SHIPPING.name().equals(optionalOrder.get().getOrderStatus()) && orderStatus.equals(OrderStatus.PENDING.name())) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_CHANGE_SHIPPING_ORDER);
        }

        if (OrderStatus.COMPLETED.name().equals(optionalOrder.get().getOrderStatus())) {
            if (orderStatus.equals(OrderStatus.PENDING.name()) || orderStatus.equals(OrderStatus.SHIPPING.name())) {
                throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_CHANGE_COMPLETE_ORDER);
            }
        }
        Order order = optionalOrder.get();
        order.setOrderStatus(orderStatus);
        orderRepository.save(order);
    }

    @Override
    public void cancelOrder(String id, String buyerId) throws NabException {
        Optional<Order> order = orderRepository.findById(id);
        if (!order.isPresent()) {
            throw new NabException(NabExceptionCode.NOT_FOUND_EXCEPTION).withMessageKey(Constants.DELETE_NON_EXIST_ORDER);
        }

        String orderStatus = order.get().getOrderStatus();
        if (OrderStatus.SHIPPING.name().equals(orderStatus) || OrderStatus.COMPLETED.name().equals(orderStatus)) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_CANCEL_ORDER_AT_THIS_MOMENT);
        }

        if (order.get().getBuyer() == null || !order.get().getBuyer().getId().equals(buyerId)) {
            throw new NabException(NabExceptionCode.FORBIDDEN_EXCEPTION).withMessageKey(Constants.COULD_NOT_CANCEL_OTHER_OF_OTHER);
        }

        orderRepository.delete(order.get());
        sendCancelOrderEvent(order.get().getOrderItems());
    }

    private void fillOrderInfor(Order order) {
        order.setOrderDate(System.currentTimeMillis());
        order.setOrderStatus(OrderStatus.PENDING.toString());
    }

    private void updateProductDemand(List<ProductDemandDTO> productDemands) throws NabException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<List<ProductDemandDTO>> entity = new HttpEntity<>(productDemands, headers);
        try {
            restTemplate.put(Constants.PRODUCT_HOST_ADDRESS + Constants.PRODUCTS_URI, entity);
        } catch (HttpStatusCodeException exception) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.COULD_NOT_CREATE_ORDER);
        }
    }

    private void sendCancelOrderEvent(Set<OrderItem> orderItems) {
        List<DeletedOrderDto> deletedOrderDtos = new ArrayList<>();
        orderItems.stream().forEach(orderItem -> deletedOrderDtos.add(new DeletedOrderDto(orderItem.getProductId(), orderItem.getUnits())));

        String dtoAsString = "";
        try {
            dtoAsString = objectMapper.writeValueAsString(deletedOrderDtos);
        } catch (JsonProcessingException e) {
            LOGGER.error("could not convert message,{}", e.getMessage());
        }
        jmsTemplate.convertAndSend(queue, dtoAsString);
    }
}
