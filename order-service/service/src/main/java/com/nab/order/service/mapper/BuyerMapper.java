/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.service.mapper;


import com.nab.order.domain.Buyer;
import com.nab.order.dto.BuyerDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class BuyerMapper {
    public static final BuyerMapper MAPPER = Mappers.getMapper(BuyerMapper.class);

    public abstract BuyerDto entityToDto(Buyer model);

    public abstract Buyer dtoToEntity(BuyerDto orderDto);
}
