/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.service.mapper;


import com.nab.order.domain.Order;
import com.nab.order.dto.OrderDto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class OrderMapper {
    public static final OrderMapper MAPPER = Mappers.getMapper(OrderMapper.class);

    public abstract OrderDto entityToDto(Order model);

    @Mapping(ignore = true, target = "paymentMethod")
    public abstract Order dtoToEntity(OrderDto orderDto);
}
