/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.dto;

import lombok.Data;

@Data
public class BuyerDto {
    private String id;
    private String name;
    private String phone;
    private String email;
    private String address;
}