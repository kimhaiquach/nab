/*
 * Copyright (c) 2020 Absolute Software Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.dto;

import java.util.stream.Stream;

public enum OrderStatus {
    PENDING("pending"),
    PROCESSING("processing"),
    SHIPPING("shipping"),
    COMPLETED("completed");

    private String status;

    private OrderStatus(String status) {
        this.status = status;
    }

    public static boolean contains(String orderStatus) {
        return Stream.of(OrderStatus.values()).anyMatch(status -> status.name().equals(orderStatus));
    }

    public String getStatus() {
        return this.status;
    }
}
