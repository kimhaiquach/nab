/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class OrderItemDto {
    private String id;
    @NotEmpty(message = "Please provide an Product Id")
    private String productId;
    @NotEmpty(message = "Please provide an Product Name")
    private String productName;
    private String pictureUrl;
    @Min(value = 0, message = "Invalid number of units")
    private BigDecimal unitPrice;

    private BigDecimal discount;
    private String orderId;
    @Min(value = 0, message = "Invalid number of units")
    private int units;
}
