/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.domain;

import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "order_table")
public class Order {
    private String id;
    private long orderDate;
    private String orderStatus;
    private String description;
    private String paymentMethod ="cash only";
    private String address;
    private Buyer buyer;
    private Set<OrderItem> orderItems;
    private BigDecimal totalPrice;

    @Id
    @Column(name = "Id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String getId() {
        return id;
    }

    public Order setId(String id) {
        this.id = id;
        return this;
    }

    @Column(name = "Order_Date")
    public long getOrderDate() {
        return orderDate;
    }

    public Order setOrderDate(long orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    @Column(name = "Order_Status")
    public String getOrderStatus() {
        return orderStatus;
    }

    public Order setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public Order setDescription(String description) {
        this.description = description;
        return this;
    }

    @Column(name = "Payment_Method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    public Order setPaymentMethod(String paymentMethod) {
        return this;
    }

    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public Order setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Order that = (Order) o;
        return id == that.id &&
                orderDate == that.orderDate &&
                Objects.equals(orderStatus, that.orderStatus) &&
                Objects.equals(description, that.description) &&
                Objects.equals(paymentMethod, that.paymentMethod) &&
                Objects.equals(address, that.address) &&
                Objects.equals(buyer, that.buyer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderDate, orderStatus, description, paymentMethod, address, buyer);
    }

    @ManyToOne
    @JoinColumn(name = "Buyer_Id", referencedColumnName = "Id", nullable = false)
    public Buyer getBuyer() {
        return buyer;
    }

    public Order setBuyer(Buyer buyer) {
        this.buyer = buyer;
        return this;
    }

    @OneToMany(targetEntity = OrderItem.class, mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public Order setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
        return this;
    }

    @Column(name = "Total_Price")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public Order setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }
}
