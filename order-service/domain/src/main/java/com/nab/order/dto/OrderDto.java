/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.dto;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class OrderDto {
    private String paymentMethod;
    private String id;
    private long orderDate;
    private String orderStatus;
    private String description;
    @NotBlank
    private String address;
    @NotNull
    private BuyerDto buyer;
    private BigDecimal totalPrice;
    @Valid @NotNull
    private Set<OrderItemDto> orderItems;
}