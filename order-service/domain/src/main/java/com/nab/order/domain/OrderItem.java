/*
 * Copyright (c) 2020 NAB. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */
package com.nab.order.domain;

import org.hibernate.annotations.GenericGenerator;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "order_item")
public class OrderItem {
    private String id;
    private String productId;
    private String productName;
    private String pictureUrl;
    private BigDecimal unitPrice;
    private BigDecimal discount;
    private int units;
    private Order order;

    @Id
    @Column(name = "Id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    public String getId() {
        return id;
    }

    public OrderItem setId(String id) {
        this.id = id;
        return this;
    }

    @Column(name = "Product_Id")
    public String getProductId() {
        return productId;
    }

    public OrderItem setProductId(String productId) {
        this.productId = productId;
        return this;
    }

    @Column(name = "Product_Name")
    public String getProductName() {
        return productName;
    }

    public OrderItem setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    @Column(name = "Picture_Url")
    public String getPictureUrl() {
        return pictureUrl;
    }

    public OrderItem setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
        return this;
    }

    @Column(name = "Unit_Price")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public OrderItem setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    @Column(name = "Discount")
    public BigDecimal getDiscount() {
        return discount;
    }

    public OrderItem setDiscount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    @Column(name = "Units")
    public int getUnits() {
        return units;
    }

    public OrderItem setUnits(int units) {
        this.units = units;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderItem that = (OrderItem) o;
        return id == that.id &&
                productId == that.productId &&
                units == that.units &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(pictureUrl, that.pictureUrl) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(discount, that.discount) &&
                Objects.equals(order, that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productId, productName, pictureUrl, unitPrice, discount, units, order);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Order_Id", referencedColumnName = "Id", nullable = false)
    public Order getOrder() {
        return order;
    }

    public OrderItem setOrder(Order order) {
        this.order = order;
        return this;
    }
}
