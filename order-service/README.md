# ORDER SERVICE
----
We will build a CRUD RESTFul APIs for a Simple Order Management System using Spring Boot 2 JPA and H2 database. Following are five REST APIs (Controller handler methods) are created for Order resource.


### INTRODUCTION
This service provides APIs to manage order in the system.

Techonology stack:
- Java 8
- Jersey + Jackson
- Spring Boot 
- Spring JPA
- H2 Embedded Database
- Lombok
- ActiveMQ
 
### BUILD
To build
```bash
mvn clean install
```
To build and publish coverage to local sonar, use `sonar-local` profile like below:
```bash
mvn clean install sonar:sonar -Psonar-local
```

### DEPLOYMENT
To start this service locally
```bash
  cd webservice
  mvn spring-boot:run
```

### RELEASE HISTORY
This section is to give a high level list of changes for every release

### USAGE
When you run this app you can access its features using several RESTful endpoints. Note - this is only a SMALL sample of the endpoints available. e.g. when running locally:

GET http://localhost:6969/order-service/orders - with header: "userId": 00000000-0000-0001-0000-000000000001 will returns listing of orders of user 

GET http://localhost:6969/order-service/orders/00000000-0000-0000-0001-000000000001  return single order by orderId

POST http://localhost:6969/order-service/orders to create new order.

Header:"userId":00000000-0000-0001-0000-000000000001 and requestBody OrderDto has data like below:
```bash
{
    "orderDate": 1591287748423,
    "paymentMethod": "Cash",
    "address": "tan binh",
    "buyer": {
      "id": "00000000-0000-0001-0000-000000000001",
      "name": "Kevil",
      "phone": "096868686868",
      "email": "kimhaiquach@mail.com",
      "address": "tan binh"
    },
    "orderItems": [
      {
        "productId": "00000000-0000-0001-0000-000000000001",
        "productName": "Iphone",
        "pictureUrl": "uri_to_iphone",
        "unitPrice": 20000000,
        "units": 2
      }
    ]
  }
```

PUT http://localhost:6969/order-service/orders/{id} to update Order status with request body like below 
```bash
"orderStatus":"COMPLETED"
```

DELETE http://localhost:6969/order-service/orders/{id} to cancel Order.

http://localhost:9999/health This returns the current health of the app, it is provided by Spring Boot Actuator. This and all other actuator endpoints that actuator provides are available immediately.

