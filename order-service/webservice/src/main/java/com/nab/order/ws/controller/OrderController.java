/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.ws.controller;

import com.nab.common.Constants;
import com.nab.common.exception.NabException;
import com.nab.common.exception.NabExceptionCode;
import com.nab.order.dto.OrderDto;
import com.nab.order.dto.OrderStatus;
import com.nab.order.service.OrderService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/orders")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    /**
     * * Returns specific product
     *
     * @param id the id of product
     * @return productDto
     */
    @GET
    @Path("/{id}")
    public Response getOrderById(@PathParam("id") String id) throws NabException {
        OrderDto orderDto = orderService.getOrderById(id);
        if (orderDto == null) {
            throw new NabException(NabExceptionCode.NOT_FOUND_EXCEPTION).withMessageKey(Constants.COULD_NOT_FOUND_ORDER);
        }

        return Response.ok().entity(orderDto).build();
    }


    /**
     * * Returns a list of orders
     *
     * * @return List of orderDtos of user
     */
    @GET
    public Response getAllOrderByBuyer(@NotBlank @HeaderParam("userId") String userId) {
        List<OrderDto> orderDtos = orderService.getOrderByBuyerId(userId);
        return Response.ok(orderDtos).build();
    }

    /**
     * * Create an order
     *
     * @param userId   the id of user
     * @param OrderDto OrderDto information.
     * @return id of order
     */
    @POST
    public Response createOrder(@NotBlank @HeaderParam("userId") String userId, @Valid @RequestBody OrderDto order) throws NabException {
        return Response.ok(orderService.createOrder(order, userId)).build();
    }

    /**
     * * update an order / this api just support by seller or shipper
     * and we should have user-role-management to check role of user to grant permission to access this url
     *
     * @param id       the id of order
     * @param userId   the id of user
     * @param orderStatus order Status.
     * @return id of order
     */
    @PUT
    @Path("/{id}")
    public Response updateOrderStatus(
            @PathParam("id") String id,
            @NotBlank @HeaderParam("userId") String userId,
            @NotBlank String orderStatus) throws NabException {
        if (OrderStatus.contains(orderStatus)) {
            throw new NabException(NabExceptionCode.BAD_REQUEST_EXCEPTION).withMessageKey(Constants.INVALID_ORDER_STATUS);
        }

        orderService.updateStatus(orderStatus, id);

        return Response.ok().build();
    }

    /**
     * * Delete an order
     *
     * @param id     the id of order
     * @param userId the id of user
     * @return id of order
     */
    @DELETE
    @Path("{id}")
    public Response cancelOrder(
            @PathParam("id") String id,
            @NotBlank @HeaderParam("userId") String userId) throws NabException {
        orderService.cancelOrder(id, userId);
        return Response.ok().build();
    }
}
