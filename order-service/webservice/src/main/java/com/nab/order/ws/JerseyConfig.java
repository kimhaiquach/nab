/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.ws;

import com.nab.common.exception.NabExceptionMapper;
import com.nab.order.ws.controller.OrderController;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.stereotype.Component;

/**
 * Created by haiquach on 2020-06-05.
 */
@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        register(NabExceptionMapper.class);
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(OrderController.class);
    }
}
