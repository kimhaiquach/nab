/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.ws.healthcheck;

import com.nab.order.repository.BuyerUserRepository;
import com.nab.order.repository.OrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * Created by haiquach on 2020-06-05.
 */

/**
 * Determines if URP Service are working correctly.
 */
@Component
public class SqlHealthCheck implements HealthIndicator {
    private static final String DETAILS = "details";

    /**
     * BuyerUser Repository
     */
    @Autowired(required = false)
    private BuyerUserRepository buyerUserRepository;

    /**
     * Order Repository
     */
    @Autowired(required = false)
    private OrderRepository orderRepository;

    /**
     * This method is using for checking the status of repository
     *
     * @return the status of sql repository
     */
    @Override
    public Health health() {
        if (orderRepository == null) {
            return Health.down().withDetail(DETAILS, "missing @Autowired: orderRepository").build();
        }
        if (buyerUserRepository == null) {
            return Health.down().withDetail(DETAILS, "missing @Autowired: buyerUserRepository").build();
        }
        return Health.up().build();
    }
}
