/*
 * Copyright (c) 2020 NAB Corporation. All rights reserved.
 * Reproduction or transmission in whole or in part, in any form, or by any means
 * (electronic, mechanical, or otherwise) is prohibited without the prior written
 * consent of the copyright owner.
 */

package com.nab.order.ws.healthcheck;

import com.nab.order.service.OrderService;
import com.nab.order.service.UserManagementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * Created by haiquach on 2020-06-05.
 */

/**
 * Determines if URP Service are working correctly.
 */
@Component
public class OrderServiceHealthCheck implements HealthIndicator {
    private static final String DETAILS = "details";

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserManagementService userManagementService;

    /**
     * This method is using for checking the status of service
     *
     * @return the status of services
     */
    @Override
    public Health health() {
        if (orderService == null) {
            return Health.down().withDetail(DETAILS, "missing @Autowired: orderService").build();
        }
        if (userManagementService == null) {
            return Health.down().withDetail(DETAILS, "missing @Autowired: buyerUserService").build();
        }

        return Health.up().build();
    }
}
