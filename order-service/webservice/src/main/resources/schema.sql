create table if not exists buyer
(
	Id varchar(36) not null
		primary key,
	Name varchar(128) not null,
	Phone varchar(45) not null,
	Email varchar(45) not null,
	Address varchar(255) null
);

create table if not exists `order_table`
(
	Id varchar(36) not null primary key,
	Order_Date bigint not null,
	Buyer_Id varchar(36) not null,
	Total_Price decimal(13,4) not null,
	Order_Status varchar(45) not null,
	Description varchar(255) null,
	Payment_Method varchar(45) not null,
	Address varchar(255) not null,
	constraint Buyer_Id
		foreign key (Buyer_Id) references buyer (Id)
);

create index Buyer_Id_idx
	on `order_table` (Buyer_Id);

create table if not exists order_item
(
	Id varchar(36) not null
		primary key,
	Order_Id varchar(36) not null,
	Product_Id varchar(36) not null,
	Product_Name varchar(45) not null,
	Picture_Url varchar(45) null,
	Unit_Price decimal(13,4) not null,
	Discount decimal(13,4) null,
	Units int not null,
	constraint fk_order_id
		foreign key (Order_Id) references `order_table` (Id)
);

create index fk_order_product_id_idx
	on order_item (Order_Id);
